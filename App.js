// status bar light
import React from "react";
import { StatusBar } from "expo-status-bar";
import { Button, StyleSheet, View } from "react-native";
import * as ImagePicker from "expo-image-picker";

import VideoPlayer from "./src/components/VideoPlayer";

export default function App() {
  const [uri, setUri] = React.useState(
    "https://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4"
  );

  function pickVideo() {
    ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Videos,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    }).then((result) => {
      // console.log(result);
      if (!result.canceled) {
        setUri(result.assets[0].uri);
      }
    });
  }

  return (
    <View style={styles.container}>
      <StatusBar style="light" />
      <Button title="Select Video" onPress={pickVideo} />
      <VideoPlayer uri={uri} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#333",
    alignItems: "stretch",
    justifyContent: "space-around",
    gap: 16,
    padding: 16,
  },
});
