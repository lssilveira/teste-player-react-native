import React from "react";
import { Button, StyleSheet, View } from "react-native";
import { Video, ResizeMode } from "expo-av";

/**
 * @param {{ uri: any; }} props
 */
export default function VideoPlayer(props) {
  // const video = React.useRef(null)
  const video = React.useRef(null);
  const [status, setStatus] = React.useState({});

  return (
    <View style={styles.container}>
      <Video
        ref={video}
        source={{
          // uri: "https://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4",
          uri: props.uri,
        }} // Can be a URL or a local file.
        style={styles.video}
        useNativeControls
        isLooping
        resizeMode={ResizeMode.CONTAIN}
        isMuted={true}
        shouldPlay={true}
        onPlaybackStatusUpdate={(status) => setStatus(() => status)}
      />
      <View>
        <Button
          title={status.isPlaying ? "Pause" : "Play"}
          onPress={() => {
            if (!video.current) {
              return;
            }

            status.isPlaying
              ? video.current.pauseAsync()
              : video.current.playAsync();
          }}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#333",
    gap: 5,
    borderRadius: 5,
    borderWidth: 1,
  },
  video: {
    height: 300,
    backgroundColor: "#111",
  },
});
